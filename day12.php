<?php

$instructions = explode(PHP_EOL, file_get_contents('../inputs/day12.txt'));

function executeInstructions($vX, $vY, $waypoint) {
    global $instructions;
    $x = $y = 0;
    foreach ($instructions as $instruction) {
        list($action, $value) = preg_split('/(?<=[NSEWLRF])(?=[0-9]+)/', $instruction);
        $value = intval($value);
        if ($action === 'N' || $action === 'S') {
            if (!$waypoint) {
                $y += $action === 'N' ? $value : -$value;
            } else {
                $vY += $action === 'N' ? $value : -$value;
            }
        } else if ($action === 'E' || $action === 'W') {
            if (!$waypoint) {
                $x += $action === 'E' ? $value : -$value;
            } else {
                $vX += $action === 'E' ? $value : -$value;
            }
        } else if ($action === 'L' || $action === 'R') {
            $angle = deg2rad($action === 'L' ? $value : 360 - $value);
            list($oldV_X, $oldV_Y) = [$vX, $vY];
            $vX = round($oldV_X * cos($angle) - $oldV_Y * sin($angle));
            $vY = round($oldV_X * sin($angle) + $oldV_Y * cos($angle));
        } else if ($action === 'F') {
            $x += $vX * $value;
            $y += $vY * $value;
        }
    }
    return abs($x) + abs($y);
}

// Part 1
echo executeInstructions(1, 0, false) . PHP_EOL;

// Part 2
echo executeInstructions(10, 1, true) . PHP_EOL;