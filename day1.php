<?php

$lines = explode(PHP_EOL, file_get_contents('../inputs/day1.txt'));

$entries = array_map('intval', $lines);

// Part 1
foreach ($entries as $a) {
    foreach ($entries as $b) {
        if ($a + $b === 2020) {
            echo $a * $b . PHP_EOL;
            break 2;
        }
    }
}

// Part 2
foreach ($entries as $a) {
    foreach ($entries as $b) {
        if ($a + $b > 2020) {
            continue;
        }
        foreach ($entries as $c) {
            if ($a + $b + $c === 2020) {
                echo $a * $b * $c . PHP_EOL;
                break 3;
            }
        }
    }
}

die;
