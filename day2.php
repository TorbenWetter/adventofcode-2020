<?php

$lines = explode(PHP_EOL, file_get_contents('../inputs/day2.txt'));

$entries = array_map(function (string $line) {
    list($policy, $password) = explode(': ', $line);
    list($range, $letter) = explode(' ', $policy);
    list($min, $max) = array_map('intval', explode('-', $range));
    return [
        'min' => $min,
        'max' => $max,
        'letter' => $letter,
        'password' => $password,
    ];
}, $lines);

// Part 1
$count = 0;
foreach ($entries as $entry) {
    extract($entry);
    $letterCount = substr_count($password, $letter);
    if ($min <= $letterCount && $letterCount <= $max) {
        $count++;
    }
}
echo $count . PHP_EOL;

// Part 2
$count = 0;
foreach ($entries as $entry) {
    extract($entry);
    if ($password[$min - 1] === $letter xor $password[$max - 1] === $letter) {
        $count++;
    }
}
echo $count . PHP_EOL;

die;