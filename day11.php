<?php

$lines = explode(PHP_EOL, file_get_contents('../inputs/day11.txt'));

$seatLayout = array_map(fn ($line) => str_split($line), $lines);
$height = sizeof($seatLayout);
$width = sizeof($seatLayout[0]);

$updated;

function updateSeats($seatLayout, $part1) {
    global $updated, $height, $width;
    $updated = false;
    $newSeatLayout = $seatLayout;
    foreach ($seatLayout as $y => $row) {
        foreach ($row as $x => $state) {
            if ($state === '.') {
                continue;
            }

            $occupied = 0;
            for ($yOff = -1; $yOff <= 1; $yOff++) {
                for ($xOff = -1; $xOff <= 1; $xOff++) {
                    if ($yOff === 0 && $xOff === 0) {
                        continue;
                    }

                    $j = $y + $yOff;
                    $i = $x + $xOff;
                    if ($part1) {
                        if ($j < 0 || $j >= $height || $i < 0 || $i >= $width) {
                            continue;
                        }

                        if ($seatLayout[$j][$i] === '#') {
                            $occupied++;
                            if ($occupied === 4) {
                                break 2;
                            }
                        }
                    } else {
                        $distance = 1;
                        while (!($j < 0 || $j >= $height || $i < 0 || $i >= $width)) {
                            if ($seatLayout[$j][$i] === 'L') {
                                break;
                            }
                            if ($seatLayout[$j][$i] === '#') {
                                $occupied++;
                                if ($occupied === 5) {
                                    break 3;
                                } else {
                                    break;
                                }
                            }

                            $distance++;
                            $j = $y + $distance * $yOff;
                            $i = $x + $distance * $xOff;
                        }
                    }
                }
            }

            if ($state === 'L' && $occupied === 0) {
                $newSeatLayout[$y][$x] = '#';
                $updated = true;
            }
            if ($state === '#' && ($part1 && $occupied === 4 || !$part1 && $occupied === 5)) {
                $newSeatLayout[$y][$x] = 'L';
                $updated = true;
            }
        }
    }
    return $newSeatLayout;
}

function countOccupied($row) {
    $counts = array_count_values($row);
    return array_key_exists('#', $counts) ? $counts['#'] : 0;
}

// Part 1
$seatLayout1 = $seatLayout;
do {
    $seatLayout1 = updateSeats($seatLayout1, true);
} while ($updated);
echo array_sum(array_map('countOccupied', $seatLayout1)) . PHP_EOL;

// Part 2
$seatLayout2 = $seatLayout;
do {
    $seatLayout2 = updateSeats($seatLayout2, false);
} while ($updated);
echo array_sum(array_map('countOccupied', $seatLayout2)) . PHP_EOL;