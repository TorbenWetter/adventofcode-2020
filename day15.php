<?php

ini_set('memory_limit', '-1');

$startingNumbers = array_map('intval', explode(',', file_get_contents('../inputs/day15.txt')));

$numbers;

function performTurn($lastNumber, $lastTurn) {
    global $numbers;
    if (!$numbers->offsetExists($lastNumber)) {
        $numbers->offsetSet($lastNumber, [$lastTurn]);
        return 0;
    }

    $beforeLastTurn = $numbers->offsetGet($lastNumber)[0];
    $numbers->offsetSet($lastNumber, [$lastTurn, $beforeLastTurn]);
    return $lastTurn - $beforeLastTurn;
}

function runGame($endTurn) {
    global $startingNumbers, $numbers;
    $numbers = new SplFixedArray($endTurn);
    foreach ($startingNumbers as $i => $startingNumber) {
        performTurn($startingNumber, $i + 1);
    }
    $lastNumber = $startingNumbers[sizeof($startingNumbers) - 1];
    $lastTurn = sizeof($startingNumbers);
    while ($lastTurn < $endTurn) {
        $lastNumber = performTurn($lastNumber, $lastTurn++);
    }
    return $lastNumber;
}

// Part 1
echo runGame(2020) . PHP_EOL;

// Part 2
echo runGame(30000000) . PHP_EOL;