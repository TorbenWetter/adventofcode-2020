<?php

$instructions = explode(PHP_EOL, file_get_contents('../inputs/day8.txt'));

function executeInstruction($operation, $argument, &$accumulator, &$index) {
    switch($operation) {
        case 'acc':
            $accumulator += intval($argument);
            $index++;
            break;
        case 'jmp':
            $index += intval($argument);
            break;
        case 'nop':
            $index++;
            break;
    }
}

// Part 1
$executed = [];
$accumulator = $index = 0;
while (!in_array($index, $executed)) {
    array_push($executed, $index);

    list($operation, $argument) = explode(' ', $instructions[$index]);

    executeInstruction($operation, $argument, $accumulator, $index);
}
echo $accumulator . PHP_EOL;

// Part 2
$terminateAccumulator;
$terminateIndex = sizeof($instructions);
foreach(range(0, $terminateIndex - 1) as $replaceIndex) {
    $executed = [];
    $accumulator = $index = 0;
    while (!in_array($index, $executed)) {
        if ($index === $terminateIndex) {
            $terminateAccumulator = $accumulator;
            break 2;
        }
        array_push($executed, $index);

        list($operation, $argument) = explode(' ', $instructions[$index]);
        if ($index === $replaceIndex && $operation !== 'acc') {
            $operation = $operation === 'jmp' ? 'nop' : 'jmp';
        }
        
        executeInstruction($operation, $argument, $accumulator, $index);
    }
}
echo $terminateAccumulator . PHP_EOL;