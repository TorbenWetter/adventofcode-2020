<?php

$lines = explode(PHP_EOL, file_get_contents('../inputs/day14.txt'));

$maskLen = 36;

function generateAddresses($floatingAddress) {
    $xPos = strpos($floatingAddress, 'X');
    if ($xPos === false) {
        return [$floatingAddress];
    }

    $replaced0 = generateAddresses(substr_replace($floatingAddress, '0', $xPos, 1));
    $replaced1 = generateAddresses(substr_replace($floatingAddress, '1', $xPos, 1));
    return array_merge($replaced0, $replaced1);
}

function initialize($part1) {
    global $lines, $maskLen;
    $binMask;
    $mem = [];
    foreach ($lines as $line) {
        if (substr($line, 0, 4) === 'mask') {
            preg_match(sprintf('/^mask \= (?<mask>[01X]{%d})$/', $maskLen), $line, $matches);
            $binMask = $matches['mask'];
        } else {
            preg_match('/^mem\[(?<address>[0-9]+)\] \= (?<value>[0-9]+)$/', $line, $matches);
            $address = intval($matches['address']);
            $value = intval($matches['value']);

            if ($part1) {
                $binValue = str_pad(strval(decbin($value)), $maskLen, '0', STR_PAD_LEFT);
                for ($i = 0; $i < $maskLen; $i++) {
                    if ($binMask[$i] !== 'X') {
                        $binValue[$i] = $binMask[$i];
                    }
                }
                $mem[$address] = bindec($binValue);
            } else {
                $floatingAddress = str_pad(strval(decbin($address)), $maskLen, '0', STR_PAD_LEFT);
                for ($i = 0; $i < $maskLen; $i++) {
                    if ($binMask[$i] !== '0') {
                        $floatingAddress[$i] = $binMask[$i];
                    }
                }
                foreach (generateAddresses($floatingAddress) as $binAddress) {
                    $mem[bindec($binAddress)] = $value;
                }
            }
        }
    }
    return array_sum($mem);
}

// Part 1
echo initialize(true) . PHP_EOL;

// Part 2
echo initialize(false) . PHP_EOL;