<?php

$lines = explode(PHP_EOL, file_get_contents('../inputs/day7.txt'));

$rules = [];
foreach ($lines as $line) {
    list($outerBag, $inner) = explode(' bags contain ', substr($line, 0, -1));
    $innerBags = [];
    foreach (explode(', ', $inner) as $innerBag) {
        if (substr($innerBag, 0, 2) !== 'no') {
            list($amount, $style, $color, ) = explode(' ', $innerBag);
            $innerBags[$style . ' ' . $color] = intval($amount);
        }
    }
    $rules[$outerBag] = $innerBags;
}

$bag = 'shiny gold';

// Part 1
$bagsToReplace = [$bag];
$outerBags = [];
while (sizeof($bagsToReplace) > 0) {
    $newBagsToReplace = [];
    foreach ($rules as $outerBag => $innerBags) {
        foreach ($bagsToReplace as $bagToReplace) {
            if (!in_array($outerBag, $outerBags) && array_key_exists($bagToReplace, $innerBags)) {
                array_push($newBagsToReplace, $outerBag);
                array_push($outerBags, $outerBag);
            }
        }
    }
    $bagsToReplace = $newBagsToReplace;
}
echo sizeof($outerBags) . PHP_EOL;

// Part 2
function countInnerBags($outerBag) {
    global $rules;
    $innerBags = $rules[$outerBag];
    $innerAmounts = array_map(fn ($innerBag, $amount) => $amount + $amount * countInnerBags($innerBag), array_keys($innerBags), $innerBags);
    return array_sum($innerAmounts);
}
echo countInnerBags($bag) . PHP_EOL;