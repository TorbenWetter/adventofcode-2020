<?php

list($line1, $line2) = explode(PHP_EOL, file_get_contents('../inputs/day13.txt'));

$startTime = intval($line1);
$buses = [];
foreach (explode(',', $line2) as $offset => $busId) {
    if ($busId !== 'x') {
        array_push($buses, [
            'offset' => $offset,
            'id' => intval($busId)
        ]);
    }
}

// Part 1
$bestId = -1;
$minWaitTime = PHP_INT_MAX;
foreach ($buses as $bus) {
    extract($bus);
    $waitTime = ceil($startTime / $id) * $id - $startTime;
    if ($waitTime < $minWaitTime) {
        $bestId = $id;
        $minWaitTime = $waitTime;
    }
}
echo $bestId * $minWaitTime . PHP_EOL;

// Part 2
// https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
function extendedEuclid($a, $b) {
    list($r, $s, $t, $u) = [1, 0, 0, 1];
    while ($b !== 0) {
        $q = intdiv($a, $b);
        list($a, $b) = [$b, $a - $q * $b];
        list($r, $t) = [$t, $r - $q * $t];
        list($s, $u) = [$u, $s - $q * $u];
    }
    return [$r, $s];
}

function positiveMod($a, $b) {
    return ($b + ($a % $b)) % $b;
}

// https://en.wikipedia.org/wiki/Chinese_remainder_theorem
$x = 0;
$M = array_product(array_map(fn ($bus) => $bus['id'], $buses));
foreach ($buses as $bus) {
    extract($bus);
    /*
    *     ($t + $offset) % $id = 0         [AOC]
    * <=> $t % $id = $id - ($offset % $id) [transformed]
    *
    *     $x % $m_i = $a_i                 [Wikipedia]
    */
    $m_i = $id;
    $a_i = $id - ($offset % $id);
    $M_i = $M / $m_i;
    list(, $s_i) = extendedEuclid($m_i, $M_i);
    $e_i = $s_i * $M_i;
    $x += $a_i * $e_i;
}
$t = positiveMod($x, $M);
echo $t . PHP_EOL;