<?php

$lines = explode(PHP_EOL, file_get_contents('../inputs/day3.txt'));
$height = sizeof($lines);

$slopes = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2]
];

function countTrees($vX, $vY) {
    global $lines, $height;
    $x = $y = $count = 0;
    while ($y < $height) {
        $line = $lines[$y];
        if ($line[$x % strlen($line)] === '#') {
            $count++;
        }
    
        $x += $vX;
        $y += $vY;
    }
    return $count;
}

// Part 1
$treeCount = countTrees(...$slopes[1]);
echo $treeCount . PHP_EOL;

// Part 2
$treeCount = 1;
foreach ($slopes as $slope) {
    $treeCount *= countTrees(...$slope);
}
echo $treeCount . PHP_EOL;